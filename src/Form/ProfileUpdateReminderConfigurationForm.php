<?php

namespace Drupal\profile_update_reminder\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines our form class.
 */
class ProfileUpdateReminderConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'profile_update_reminder_configuration_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'profile_update_reminder_configuration_form.profile_update_reminder_configuration_form_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('profile_update_reminder_configuration_form.profile_update_reminder_configuration_form_settings');

    if (!empty($form_state->getValue(['profile_update_fieldset', 'field_count']))) {
      $defaultCount = $form_state->getValue(['profile_update_fieldset', 'field_count']);
    }
    else {
      $defaultCount = $config->get('field_count') ?: 1;
    }

    $yesNoOPtions = [
      'yes' => $this->t('Yes'),
      'no' => $this->t('no'),
    ];

    $form['#tree'] = TRUE;

    $form['profile_delay_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Time Options'),
    ];

    $form['profile_delay_fieldset']['reminder_delay'] = [
      '#type' => 'number',
      '#title' => $this->t('Time Till Next Reminder'),
      '#required' => TRUE,
      '#description' => $this->t(
        '<p>Choose how long to wait. Value in seconds.</p>
        <ul>
        <li>1 day: 86400</li>
        <li>7 days: 604800</li>
        <li>30 days: 2592000</li>
        </ul>'
      ),
      '#default_value' => $config->get('reminder_delay') ?: 604800,
    ];

    $form['profile_update_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Field Options'),
      '#prefix' => '<div id="profile-update-fiedlset-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['profile_update_fieldset']['field_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of Fields'),
      '#required' => TRUE,
      '#description' => $this->t(
        '<p>Provide the number of field items below.</p>
        <ul>
        <li>The field names below will be tracked for reminders</li>
        </ul>'
      ),
      '#default_value' => $config->get('field_count') ?: 1,
    ];

    $form['profile_update_fieldset']['rebuild'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply number'),
      '#submit' => ['::rebuildFormSubmit'],
      '#ajax' => [
        'callback' => '::fieldNameCallback',
        'wrapper' => 'profile-update-fiedlset-wrapper',
      ],
    ];

    $form['profile_update_fieldset']['spacer'] = [
      '#type' => 'item',
      '#markup' => $this->t('<br /><hr /><br />'),
    ];

    for ($i = 0; $i < $defaultCount; $i++) {

      $form['profile_update_fieldset']['field_name'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Field Name'),
        '#required' => FALSE,
        '#default_value' => isset($config->get('field_name')[$i]) ? $config->get('field_name')[$i] : '',
      ];

    }

    $form['profile_options_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Display Options'),
    ];

    $form['profile_options_fieldset']['form_message_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form Message Text'),
      '#description' => $this->t('<p>Set the text for the form message. Does not change field names display.</p>'),
      '#default_value' => $config->get('form_message_text') ?: 'The following profile field(s) need to be completed: ',
    ];

    $form['profile_options_fieldset']['show_empty_fields_list'] = [
      '#type' => 'radios',
      '#title' => $this->t('Show Empty Field List in Message?'),
      '#required' => TRUE,
      '#options' => $yesNoOPtions,
      '#description' => $this->t('<p>Choose to show the empty fields list in the form.</p>'),
      '#default_value' => $config->get('show_empty_fields_list') ?: 'yes',
    ];

    $form['profile_options_fieldset']['allow_profile_link'] = [
      '#type' => 'radios',
      '#title' => $this->t('Allow Profile Link?'),
      '#required' => TRUE,
      '#options' => $yesNoOPtions,
      '#description' => $this->t('<p>Choose to show the profile link button.</p>'),
      '#default_value' => $config->get('allow_profile_link') ?: 'yes',
    ];

    $form['profile_options_fieldset']['update_profile_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Profile Submit Text'),
      '#description' => $this->t('<p>Set the text for the profile link button.</p>'),
      '#default_value' => $config->get('update_profile_text') ?: "Update profile",
      '#states' => [
        'visible' => [
          ':input[name="profile_options_fieldset[allow_profile_link]"]' => ['value' => 'yes'],
        ],
        'required' => [
          ':input[name="profile_options_fieldset[allow_profile_link]"]' => ['value' => 'yes'],
        ],
      ],
    ];

    $form['profile_options_fieldset']['allow_delay'] = [
      '#type' => 'radios',
      '#title' => $this->t('Allow Delay Submit?'),
      '#required' => TRUE,
      '#options' => $yesNoOPtions,
      '#description' => $this->t('<p>Choose to show the delay submit button.</p>'),
      '#default_value' => $config->get('allow_delay') ?: 'yes',
    ];

    $form['profile_options_fieldset']['delay_submit_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delay Submit Text'),
      '#description' => $this->t('<p>Set the text for the delay submit button.</p>'),
      '#default_value' => $config->get('delay_submit_text') ?: 'Remind me later',
      '#states' => [
        'visible' => [
          ':input[name="profile_options_fieldset[allow_delay]"]' => ['value' => 'yes'],
        ],
        'required' => [
          ':input[name="profile_options_fieldset[allow_delay]"]' => ['value' => 'yes'],
        ],
      ],
    ];

    $form['profile_options_fieldset']['allow_ignore'] = [
      '#type' => 'radios',
      '#title' => $this->t('Allow Ignore Submit?'),
      '#required' => TRUE,
      '#options' => $yesNoOPtions,
      '#description' => $this->t('<p>Choose to show the ignore submit button.</p>'),
      '#default_value' => $config->get('allow_ignore') ?: 'yes',
    ];

    $form['profile_options_fieldset']['ignore_submit_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ignore Submit Text'),
      '#description' => $this->t('<p>Set the text for the ignore submit button.</p>'),
      '#default_value' => $config->get('ignore_submit_text') ?: "Don't remind me",
      '#states' => [
        'visible' => [
          ':input[name="profile_options_fieldset[allow_ignore]"]' => ['value' => 'yes'],
        ],
        'required' => [
          ':input[name="profile_options_fieldset[allow_ignore]"]' => ['value' => 'yes'],
        ],
      ],
    ];

    $form['profile_other_options_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Other Options'),
    ];

    $form['profile_other_options_fieldset']['use_module_css'] = [
      '#type' => 'radios',
      '#title' => $this->t('Use Module CSS?'),
      '#required' => TRUE,
      '#options' => $yesNoOPtions,
      '#description' => $this->t('<p>Use the module provided CSS.</p>'),
      '#default_value' => $config->get('use_module_css') ?: 'yes',
    ];

    $form['profile_other_options_fieldset']['show_user_form_as_message'] = [
      '#type' => 'radios',
      '#title' => $this->t('Show Reminder as Drupal Warning Message?'),
      '#required' => TRUE,
      '#options' => $yesNoOPtions,
      '#description' => $this->t(
        '<p>Choose to show the reminder as a Drupal message.</p>
        <ul>
        <li>This is the reminder message only and not the form</li> 
        <li>It will follow Drupal message protocols and will be <strong>persistent without added functionality</strong></li>
        </ul>'
      ),
      '#default_value' => $config->get('show_user_form_as_message') ?: 'no',
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function fieldNameCallback(array &$form, FormStateInterface $form_state) {
    return $form['profile_update_fieldset'];
  }

  /**
   * {@inheritdoc}
   */
  public function rebuildFormSubmit(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    $this->configFactory->getEditable('profile_update_reminder_configuration_form.profile_update_reminder_configuration_form_settings')
      ->set('reminder_delay', $values['profile_delay_fieldset']['reminder_delay'])
      ->set('field_count', $values['profile_update_fieldset']['field_count'])
      ->set('field_name', $values['profile_update_fieldset']['field_name'])
      ->set('form_message_text', $values['profile_options_fieldset']['form_message_text'])
      ->set('show_empty_fields_list', $values['profile_options_fieldset']['show_empty_fields_list'])
      ->set('allow_profile_link', $values['profile_options_fieldset']['allow_profile_link'])
      ->set('update_profile_text', $values['profile_options_fieldset']['update_profile_text'])
      ->set('allow_delay', $values['profile_options_fieldset']['allow_delay'])
      ->set('delay_submit_text', $values['profile_options_fieldset']['delay_submit_text'])
      ->set('allow_ignore', $values['profile_options_fieldset']['allow_ignore'])
      ->set('ignore_submit_text', $values['profile_options_fieldset']['ignore_submit_text'])
      ->set('use_module_css', $values['profile_other_options_fieldset']['use_module_css'])
      ->set('show_user_form_as_message', $values['profile_other_options_fieldset']['show_user_form_as_message'])
      ->save();

    parent::submitForm($form, $form_state);

  }

}
