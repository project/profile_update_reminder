<?php

namespace Drupal\profile_update_reminder\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines our form class.
 */
class ProfileUpdateReminderDisableForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'profile_update_reminder_disable_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'profile_update_reminder_disable_form.profile_update_reminder_disable_form_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['warning'] = [
      '#type' => 'item',
      '#markup' => '*Caution, there should be good reason to use this form.',
    ];

    // Fieldset item.
    $form['disable_single_update_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Disable For Single User'),
    ];

    $form['disable_single_update_fieldset']['disable_note_single'] = [
      '#type' => 'item',
      '#markup' => $this->t('The update form will NOT show for this single user.'),
    ];

    $form['disable_single_update_fieldset']['disable_single_user'] = [
      '#type' => 'number',
      '#title' => $this->t('Enter a Single User ID'),
      '#required' => FALSE,
      '#default_value' => '',
    ];

    $form['disable_single_update_fieldset']['disable_single_user_sibmit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Disable Single User'),
      '#submit' => ['::disableSingleUser'],
    ];

    // Fieldset item.
    $form['disable_all_update_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Disable For All Users'),
    ];

    $form['disable_all_update_fieldset']['disable_note_all'] = [
      '#type' => 'item',
      '#markup' => $this->t('The update form will NOT show for all users.'),
    ];

    $form['disable_all_update_fieldset']['disable_all_users'] = [
      '#type' => 'submit',
      '#value' => $this->t('Disable All Users'),
      '#submit' => ['::disableAllUsers'],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function disableSingleUser(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    // Connect to database.
    $database = \Drupal::database();

    // Insert/Update our database data.
    $database->merge('users_data')
      ->key([
        'uid' => $values['disable_single_user'],
        'name' => 'update_reminder_timestamp',
      ])
      ->updateFields([
        'value' => 0,
      ])
      ->execute();

    parent::submitForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function disableAllUsers(array &$form, FormStateInterface $form_state) {

    // Connect to database.
    $database = \Drupal::database();

    // Insert/Update our database data.
    $database->merge('users_data')
      ->key([
        'module' => 'profile_update_reminder',
        'name' => 'update_reminder_timestamp',
      ])
      ->updateFields([
        'value' => 0,
      ])
      ->execute();

    parent::submitForm($form, $form_state);

  }

}
