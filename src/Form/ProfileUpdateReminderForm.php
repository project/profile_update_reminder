<?php

namespace Drupal\profile_update_reminder\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

/**
 * Defines our form class.
 */
class ProfileUpdateReminderForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'profile_update_reminder_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'profile_update_reminder_form.profile_update_reminder_form_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $sRF = pur_showReminderForm();
    $fCV = pur_formGeneralConfigValues();
    $eFE = pur_emptyFieldsExist();
    $gITD = pur_getInteractTimestampData();
    $mM = pur_messageMarkup();

    // Form description item.
    $form['description'] = [
      '#type' => 'item',
      '#markup' => $mM,
    ];

    // User data timestamp item.
    $form['interact_timestamp'] = [
      '#type' => 'hidden',
      '#default_value' => $gITD,
    ];

    if ($fCV['allowProfileLink'] == 'yes') {
      // Field update profile item.
      $form['access_timestamp_profile_link'] = [
        '#type' => 'submit',
        '#value' => $this->t($fCV['updateProfileText']),
        '#submit' => ['::pur_profileRedirect'],
        '#attributes' => [
          'class' => [
            'profile-update-link',
          ],
        ],
      ];
    }

    if ($fCV['allowDelay'] == 'yes') {
      // Field reminder delay item.
      $form['access_timestamp_delay'] = [
        '#type' => 'submit',
        '#value' => $this->t($fCV['delaySubmitText']),
        '#submit' => ['::pur_submitFormDelay'],
        '#attributes' => [
          'class' => [
            'delay-reminder',
          ],
        ],
      ];
    }

    if ($fCV['allowIgnore'] == 'yes') {
      // Field reminder ignore item.
      $form['access_timestamp_ignore'] = [
        '#type' => 'submit',
        '#value' => $this->t($fCV['ignoreSubmitText']),
        '#submit' => ['::pur_submitFormIgnore'],
        '#attributes' => [
          'class' => [
            'ignore-reminder',
          ],
        ],
      ];
    }
  
    $form['show_user_form'] = [
      '#type' => 'hidden',
      '#default_value' => $sRF,
    ];

    $form['empty_fields_exist'] = [
      '#type' => 'hidden',
      '#default_value' => $eFE['emptyFieldsExist'],
    ];

    $form['use_module_css'] = [
      '#type' => 'hidden',
      '#default_value' => $fCV['useModuleCSS'],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function pur_profileRedirect(array &$form, FormStateInterface $form_state) {

    // Create new redirect response to user edit.
    $response = new RedirectResponse(pur_profileUpdateLink()['base_url'] . pur_profileUpdateLink()['path']);

    // Send response.
    $response->send();

  }

  /**
   * {@inheritdoc}
   */
  public function pur_submitFormDelay(array &$form, FormStateInterface $form_state) {

    // Set our user data value.
    pur_getUserData()->set('profile_update_reminder', pur_getUser()['user_id'], 'update_reminder_timestamp', time(), 0);

  }

  /**
   * {@inheritdoc}
   */
  public function pur_submitFormIgnore(array &$form, FormStateInterface $form_state) {

    // Set our user data value.
    pur_getUserData()->set('profile_update_reminder', pur_getUser()['user_id'], 'update_reminder_timestamp', 0, 0);

  }

}
