<?php

namespace Drupal\profile_update_reminder\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines our form class.
 */
class ProfileUpdateReminderEnableForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'profile_update_reminder_enable_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'profile_update_reminder_enable_form.profile_update_reminder_enable_form_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['warning'] = [
      '#type' => 'item',
      '#markup' => '*Caution, there should be good reason to use this form.',
    ];

    // Fieldset item.
    $form['enable_single_update_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Enable For Single User'),
    ];

    $form['enable_single_update_fieldset']['disable_note_single'] = [
      '#type' => 'item',
      '#markup' => $this->t('The update form WILL show for this single user.'),
    ];

    $form['enable_single_update_fieldset']['enable_single_user'] = [
      '#type' => 'number',
      '#title' => $this->t('Enter a Single User ID'),
      '#required' => FALSE,
      '#default_value' => '',
    ];

    $form['enable_single_update_fieldset']['enable_single_user_sibmit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Enable Single User'),
      '#submit' => ['::enableSingleUser'],
    ];

    // Fieldset item.
    $form['enable_all_update_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Enable For All Users'),
    ];

    $form['enable_all_update_fieldset']['disable_note_all'] = [
      '#type' => 'item',
      '#markup' => $this->t('The update form WILL show for all users.'),
    ];

    $form['enable_all_update_fieldset']['enable_all_users'] = [
      '#type' => 'submit',
      '#value' => $this->t('Enable All Users'),
      '#submit' => ['::enableAllUsers'],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function enableSingleUser(array &$form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    // Connect to database.
    $database = \Drupal::database();

    // Insert/Update our database data.
    $database->merge('users_data')
      ->key([
        'uid' => $values['enable_single_user'],
        'name' => 'update_reminder_timestamp',
      ])
      ->updateFields([
        'value' => 1,
      ])
      ->execute();

    parent::submitForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function enableAllUsers(array &$form, FormStateInterface $form_state) {

    // Connect to database.
    $database = \Drupal::database();

    // Insert/Update our database data.
    $database->merge('users_data')
      ->key([
        'module' => 'profile_update_reminder',
        'name' => 'update_reminder_timestamp',
      ])
      ->updateFields([
        'value' => 1,
      ])
      ->execute();

    parent::submitForm($form, $form_state);

  }

}
