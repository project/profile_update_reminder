<?php

namespace Drupal\profile_update_reminder\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides the profile update reminder form.
 *
 * @Block(
 *   id = "profile_update_reminder_block",
 *   admin_label = @Translation("Profile Update Reminder Block"),
 *   category = @Translation("Forms"),
 * )
 */
class ProfileUpdateReminderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (pur_getReminderForm()['show_user_form']['#default_value'] == 'yes') {
      return [
        pur_getReminderForm(),
      ];
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['profile_update_reminder_block_settings'] = $form_state->getValue('profile_update_reminder_block_settings');
  }

}
