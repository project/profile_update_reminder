INTRODUCTION
------------

# Profile Update Reminder

## Description
Bug your users to complete their profile.
This module reminds users to fill in empty profile fields.
This module writes to the users_data table to store a time value we can compare with. Checks for empty (tracked) user fields, and other time-based calculations to see whether the user should be notified with the form.
Form provides options to update profile, remind them later, or ignore the reminder completely. Admins have options to set the time delay, as well as reset the users_data values if needed.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/profile_update_reminder

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/node/add/project-issue/profile_update_reminder


REQUIREMENTS
------------

This module requires the following modules:

  * Block
  * User


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.
   
1. **Install** this module


CONFIGURATION
------------

2. Configure at: /admin/config/tinsel-suite/profile-update-reminder


MODULE CSS
-----------

This module uses minimal CSS, just enough to make it work out of the box in most use cases.


MAINTAINERS
-----------

Current maintainers:
 * Preston Schmidt - https://www.drupal.org/user/3594865
