<?php

/**
 * @file
 * Bug your users to fill in their profile fields.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\profile_update_reminder\Form\ProfileUpdateReminderForm;
use Drupal\user\Entity\User;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

function pur_profileUpdateLink() {

  // Base site URL.
  global $base_url;

  // Set route for user edit.
  $path = Url::fromRoute('entity.user.edit_form', ['user' => pur_getUser()['user_id']])->toString();

  return [
    'path' => $path,
    'base_url' => $base_url,
  ];

}

/**
 * Gets the Profile Update Reminder form.
 *
 * @inheritdoc
 */
function pur_getReminderForm() {
  return \Drupal::formBuilder()->getForm(ProfileUpdateReminderForm::class);
}

/**
 * {@inheritdoc}
 */
function pur_getUser() {

  // Get current user ID.
  $userID = \Drupal::currentUser()->id();

  // Load user by current ID.
  $user = User::load($userID);

  return [
    'user_id' => $userID,
    'user' => $user,
  ];

}

/**
 * {@inheritdoc}
 */
function pur_getUserData() {

  // Get our user data service.
  return \Drupal::service('user.data');

}

/**
 * {@inheritdoc}
 */
function pur_getInteractTimestampData() {

  // Get our user interaction timestamp value.
  return pur_getUserData()->get('profile_update_reminder', pur_getUser()['user_id'], 'update_reminder_timestamp');

}

/**
 * {@inheritdoc}
 */
function pur_getGeneralConfigForm() {
  return \Drupal::config('profile_update_reminder_configuration_form.profile_update_reminder_configuration_form_settings');
}

/**
 * {@inheritdoc}
 */
function pur_formGeneralConfigValues() {

  // Get form config.
  $gFC = pur_getGeneralConfigForm();

  // Get field values.
  $allowProfileLink = $gFC->get('allow_profile_link') ?: 'yes';
  $allowDelay = $gFC->get('allow_delay') ?: 'yes';
  $allowIgnore = $gFC->get('allow_ignore') ?: 'yes';
  $formMessageText = $gFC->get('form_message_text') ?: 'The following profile field(s) need to be completed:';
  $updateProfileText = $gFC->get('update_profile_text') ?: 'Update your profile';
  $delaySubmitText = $gFC->get('delay_submit_text') ?: 'Remind me later';
  $ignoreSubmitText = $gFC->get('ignore_submit_text') ?: "Don't remind me";
  $showEmptyFieldList = $gFC->get('show_empty_fields_list') ?: 'yes';
  $fieldName = $gFC->get('field_name') ?: '';
  $reminderDelay = $gFC->get('reminder_delay') ?: 604800;
  $useModuleCSS = $gFC->get('use_module_css') ?: 'yes';
  $showReminderAsMessage = $gFC->get('show_user_form_as_message') ?: 'no';

  return [
    'allowProfileLink' => $allowProfileLink,
    'allowDelay' => $allowDelay,
    'allowIgnore' => $allowIgnore,
    'formMessageText' => $formMessageText,
    'updateProfileText' => $updateProfileText,
    'delaySubmitText' => $delaySubmitText,
    'ignoreSubmitText' => $ignoreSubmitText,
    'showEmptyFieldList' => $showEmptyFieldList,
    'fieldName' => $fieldName,
    'reminderDelay' => $reminderDelay,
    'useModuleCSS' => $useModuleCSS,
    'showReminderAsMessage' => $showReminderAsMessage,
  ];

}

/**
 * {@inheritdoc}
 */
function pur_emptyFieldsExist() {

  $emptyFieldsExist = 'no';
  $emptyFieldList = [];

  if (pur_formGeneralConfigValues()['fieldName'] != '') {

    // Loop through our tracked fields.
    foreach (pur_formGeneralConfigValues()['fieldName'] as $key) {

      // Check if any fields are empty.
      if (pur_getUser()['user']->$key->isEmpty()) {
        $emptyFieldsExist = 'yes';

        // Build our field name list.
        $emptyFieldList[] = pur_getUser()['user']->$key->getFieldDefinition()->getLabel();
      }

    }

  }

  return [
    'emptyFieldsExist' => $emptyFieldsExist,
    'emptyFieldList' => $emptyFieldList,
  ];

}

/**
 * {@inheritdoc}
 */
function pur_showReminderForm() {

  $showUserFormState = 'no';

  // Check there are empty fields.
  if (pur_emptyFieldsExist()['emptyFieldsExist'] == 'yes') {

    // Verify our user data time value.
    $timeInteraction = pur_getInteractTimestampData() ?: 0;

    // Check the time.
    if ($timeInteraction != 0) {
      if ($timeInteraction < (time() - pur_formGeneralConfigValues()['reminderDelay'])) {
        $showUserFormState = 'yes';
      }
    }

  }

  return $showUserFormState;

}

/**
 * {@inheritdoc}
 */
function pur_messageMarkup() {

  $fCV = pur_formGeneralConfigValues();
  $eFE = pur_emptyFieldsExist();

  // Below if/else is to build our form message.
  if ($fCV['showEmptyFieldList'] == 'yes') {

    // Create our message.
    $markup = new TranslatableMarkup(
      '<p class="update-reminder-message">@messageText<span class="update-reminder-empty-fields">@fieldLabels</span>.</p>', [
        '@messageText' => t($fCV['formMessageText']),
        '@fieldLabels' => t(implode(', ', $eFE['emptyFieldList'])),
      ]
    );

  }
  else {

    // Create our message.
    $markup = new TranslatableMarkup(
      '<p class="update-reminder-message">@messageText</p>', [
        '@messageText' => t($fCV['formMessageText']),
      ]
    );

  }

  return $markup;

}

/**
 * Implements hook_preprocess_HOOK().
 *
 * @inheritdoc
 */
function profile_update_reminder_preprocess_page(&$variables) {

  if (pur_getInteractTimestampData() == NULL) {
    pur_getUserData()->set('profile_update_reminder', pur_getUser()['user_id'], 'update_reminder_timestamp', time(), 0);
  }

  // Check if we should show a Drupal message.
  if (pur_formGeneralConfigValues()['showReminderAsMessage'] == 'yes') {

    // Get current theme name.
    $currentTheme = \Drupal::theme()->getActiveTheme()->getName();

    // Get default theme name.
    $defaultTheme = \Drupal::config('system.theme')->get('default');

    // Check if current and default theme names match.
    if ($currentTheme == $defaultTheme) {

      $fCV = pur_formGeneralConfigValues();
      $eFE = pur_emptyFieldsExist();

      $messageMarkup = new TranslatableMarkup(
        '<p class="update-reminder-message">@messageText<span class="update-reminder-empty-fields">@fieldLabels</span>. Please <a href="@profileEditLink">update your Profile</a>.</p>', [
          '@messageText' => t($fCV['formMessageText']),
          '@fieldLabels' => t(implode(', ', $eFE['emptyFieldList'])),
          '@profileEditLink' => pur_profileUpdateLink()['path'],
        ]
      );

      // Set our Drupal message.
      \Drupal::messenger()->addMessage($messageMarkup, MessengerInterface::TYPE_WARNING);

    }
    
  }

  // Check if we should show the form.
  if (pur_getReminderForm()['show_user_form']['#default_value'] == 'yes') {
    // Provide the form as Twig variable.
    $variables['profileUpdateReminderForm'] = pur_getReminderForm();
  }

  // Check if we should use module css.
  if (pur_getReminderForm()['use_module_css']['#default_value'] == 'yes') {
    // Attach our library.
    $variables['#attached']['library'][] = 'profile_update_reminder/styles';
  }

}

/**
 * Implements hook_preprocess_HOOK().
 *
 * @inheritdoc
 */
function profile_update_reminder_preprocess_html(&$variables) {

  // Add body class based on if user has empty fields.
  if (pur_getReminderForm()['empty_fields_exist']['#default_value'] == 'yes') {
    $variables['attributes']['class'][] = 'profile-incomplete';
  }
  elseif (pur_getReminderForm()['empty_fields_exist']['#default_value'] == 'no') {
    $variables['attributes']['class'][] = 'profile-complete';
  }

}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @inheritdoc
 */
function profile_update_reminder_form_field_config_delete_form_alter(&$form, FormStateInterface $form_state, $form_id) {

  if (\Drupal::routeMatch()->getRouteName() == 'entity.field_config.user_field_delete_form') {

    $fieldDeleteName = $form_state->getFormObject()->getEntity()->getName();

    // Loop through our tracked fields.
    foreach (pur_formGeneralConfigValues()['fieldName'] as $fieldName) {

      if ($fieldName == $fieldDeleteName) {

        // Create our message.
        $warningMessage = new TranslatableMarkup(
          'The field "<strong>' . $fieldDeleteName . '</strong>" is tracked by the "<strong>Profile Update Reminder</strong>" module. 
          Please first remove this field from the field list at <a href="/admin/config/tinsel-suite/profile-update-reminder">/admin/config/tinsel-suite/profile-update-reminder</a>.
          This is a warning and does not block the field deletion.'
        );

        \Drupal::messenger()->addMessage($warningMessage, MessengerInterface::TYPE_ERROR);

      }

    }

  }

}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @inheritdoc
 */
function profile_update_reminder_form_user_form_alter(&$form, FormStateInterface $form_state, $form_id) {

  // Loop through our tracked fields.
  foreach (pur_formGeneralConfigValues()['fieldName'] as $fieldName) {

    // Look for our tracked fields.
    if (in_array($fieldName, $form)) {

      // Add reminder empty class if field is empty.
      if (pur_getUser()['user']->$fieldName->isEmpty()) {
        $form[$fieldName]['#attributes']['class'][] = 'reminder reminder-empty';
      }
      else {
        // Add default reminder class.
        $form[$fieldName]['#attributes']['class'][] = 'reminder';
      }

    }

  }

}

/**
 * Implements hook_help().
 *
 * @inheritdoc
 */
function profile_update_reminder_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.profile_update_reminder':
      $text = file_get_contents(dirname(__FILE__) . '/README.md');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}
